import tweepy
from twitter_output_handler import TwitterOutputHandler
from datetime import date, timedelta
import time


class TwitterExtractor:

    # API Credentials
    consumer_key = 'xxxxx'
    consumer_secret = 'xxxxx'
    access_token = 'xxxxx'
    access_token_secret = 'xxxxx'
    
    def __init__(self, hashtags_file, dates_file, output_handler, max_tweets=999999):
        # hashtags: list of hashtags, str type
        # init_date: initial date for extraction, str type
        # end_date: initial date for extraction, str type
        # max_tweets: maximum extraction count, number
        auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret)
        auth.set_access_token(self.access_token, self.access_token_secret)
        self._api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
        self._max_tweets = max_tweets
        self._dates = self._process_dates(dates_file)
        self._hashtags = self._process_hashtags(hashtags_file)
        self._output_handler = output_handler

    def _process_hashtags(self, hashtags_file):
        hashtags = []
        with open(hashtags_file, 'r') as file:
            for line in file:
                hashtags.append(line.split('\n')[0])
        return hashtags
    
    def _process_dates(self, dates_file):
        with open(dates_file, 'r') as file:
            init_date = file.readline().split('\n')[0].split('-')
            init_date = date(int(init_date[0]), int(init_date[1]), int(init_date[2]))
            end_date = file.readline().split('\n')[0].split('-')
            end_date = date(int(end_date[0]), int(end_date[1]), int(end_date[2]))
        delta = end_date - init_date
        dates = []
        last_date = date(init_date.year, init_date.month, init_date.day)
        for i in range(delta.days):
            next_day = init_date + timedelta(days=i+1)
            dates.append((str(last_date), str(next_day)))
            last_date = next_day
        return dates
    
    def _extract_from_hashtag(self, hashtag, init_date, end_date):
        tweets = []
        n = 0
        print(hashtag, init_date, end_date)
        try:
            for tweet in tweepy.Cursor(self._api.search, q=hashtag, count=self._max_tweets,
                               lang="en",
                               since=init_date,
                               until=end_date,
                               tweet_mode="extended").items():
                tweets.append(tweet)
                n += 1
                if n==self._max_tweets:
                   break
                self._output_handler.write(tweet)
        except tweepy.error.TweepError as e:
            # Some kind of connection error occurred (as 503 response code
            # Sleep and wait
            time.sleep(60)
    
    def extract(self):
        for hashtag in self._hashtags:
            for init_date, end_date in self._dates:
                self._output_handler.init_output(hashtag, init_date, end_date)
                self._extract_from_hashtag(hashtag, init_date, end_date)
                self._output_handler.end_output(hashtag, init_date, end_date)

    

if __name__ == '__main__':
    #output_handler = TwitterOutputHandler(output='io')
    output_handler = TwitterOutputHandler(output='csv')
    twitter_extractor = TwitterExtractor(hashtags_file='hashtags.txt', dates_file='dates.txt', output_handler=output_handler, max_tweets=999999)
    twitter_extractor.extract()