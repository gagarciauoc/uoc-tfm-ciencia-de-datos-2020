import pandas as pd
import re
import datetime
import argparse
import os
import sys


class TwitterCSVProcessor:

    def __init__(self, input_csv_path):
        self._input_csv_path = input_csv_path
        self._data_df = pd.DataFrame()
        self._date = None
        self._process()
    
    def to_csv(self, output_file_path):
        self._data_df.to_csv(output_file_path, index=False)

    def data(self):
        return self._data_df
        
    def _process(self):
        input = open(self._input_csv_path, 'r')
        tweets = []
        count = 0
        for line in input:
            # Cleaning the tweets:
            # As they are bytes strings, they appear within b''
            # We also separate fields and field names from field values
            # using the the $SEP$ we introduced when extracting the data
            line = line.replace(",b'tweet'.",'$FIELDSEP$tweet.')
            line = line.replace(",b'tweet.",'$FIELDSEP$tweet.')
            line = line.replace(",\"b'tweet'.",'$FIELDSEP$tweet.')
            line = line.replace(",\"b'tweet.",'$FIELDSEP$tweet.')
            line = re.sub("^([ ]|[$]b)'\.", r"\1.", line)
            line = line.replace("'.b'",'.')
            line = line.replace(".b'",'.')
            line = line.replace("'$SEP$",'$SEP$')
            line = line.replace('"""','"')
            line = line.replace('""','"')
            line = re.sub("([^']), 'id", r"\1', 'id", line)
            line = re.sub("'id': ([^'])", r"'id': '\1", line)
            line = line.replace('"$FIELDSEP$', '$FIELDSEP$')
            # We get the cleaned fields in the form fieldname$SEP$fieldvalue
            fields = line.split('$FIELDSEP$')
            # The first field is: "Date,#hashtag". Special treatment:
            self._date, hashtag = fields[0].split(',')
            # Now we introduce prepare the tweet for the dataframe:
            tweet = {'file_date': self._date}
            for field in fields[1:]:
                field_name, field_value = field.split('$SEP$')
                if field_value.startswith('b"') and field_value[-1]!='"':
                    field_value += '"'
                field_value = re.sub("\]'\"", r"]'", field_value)
                field_value = re.sub('\]$', r']"', field_value)
                tweet[field_name] = field_value
            tweets.append(tweet)
            count += 1
        # After processing all the tweets, we write to a DataFrame
        self._data_df = self._data_df.append(tweets, ignore_index=True)
        # Get rid of unnamed columns (mostly index columns from original extractor data
        self._data_df = self._data_df.loc[:, ~self._data_df.columns.str.contains('^Unnamed')]


def run_processor(csv_input_path, csv_output_path):
    print('Begin: {}'.format(datetime.datetime.now()))
    processor = TwitterCSVProcessor(csv_input_path)
    tweets_df = processor.data()
    print('End process: {}'.format(datetime.datetime.now()))
    print('Begin export: {}'.format(datetime.datetime.now()))
    tweets_df.to_csv(csv_output_path)
    print('End export: {}'.format(datetime.datetime.now()))


if __name__ == '__main__':
    # Create the parser
    my_parser = argparse.ArgumentParser(description='Process a CSV file containing Tweets raw data')
    # Add the arguments
    my_parser.add_argument('input_csv',
                           metavar='Input csv file',
                           type=str,
                           help='The filename of the input CSV containing the raw data')
    my_parser.add_argument('output_csv',
                           metavar='Output csv file',
                           type=str,
                           help='The filename of the output CSV that will hold the processed Tweets data')
    # Execute the parse_args() method
    args = my_parser.parse_args()

    csv_input_path = args.input_csv
    csv_output_path = args.output_csv

    if not os.path.isfile(csv_input_path):
        print('The input file specified does not exist:\n{}'.format(csv_input_path))
        sys.exit()

    out_path = os.path.split(os.path.abspath(csv_output_path))[0]
    if not os.path.isdir(out_path):
        print('The output path specified does not exist:\n{}'.format(out_path))
        sys.exit()
        
    print('\nInput file: {}'.format(csv_input_path))
    print('Output file: {}'.format(csv_output_path))
    print('-'*40 + '\nProcessing...')
    run_processor(csv_input_path, csv_output_path)


