import pandas as pd


def extract_users_info(csv_files, output_file, field_key, fields=None):
    users_df = pd.DataFrame()
    for file in csv_files:
        current_df = pd.read_csv(file, dtype={'tweet.user.id_str': 'str', 'tweet.retweeted_status.user.id_str': 'str'})
        if fields is not None:
            current_users = current_df[[field for field in fields]].copy()
        else:
            current_users = current_df[:].copy()
        print(len(current_users))
        users_df = users_df.append(current_users, ignore_index=True)
    users_df = users_df.drop_duplicates(field_key, keep='last')
    # Get rid of unnamed columns (mostly index columns from original extractor data
    users_df = users_df.loc[:, ~users_df.columns.str.contains('^Unnamed')]
    print(len(users_df))
    users_df.to_csv(output_file, index=False)
    
if __name__ == '__main__':
    # Create a CSV file containing all users.
    csv_files = ["C:\\Python\\tfm\\output\cleaned\\2020-10-28-cleaned.csv",
                 "C:\\Python\\tfm\\output\cleaned\\2020-10-29-cleaned.csv",
                 "C:\\Python\\tfm\\output\cleaned\\2020-10-30-cleaned.csv",
                 "C:\\Python\\tfm\\output\cleaned\\2020-10-31-cleaned.csv",
                 "C:\\Python\\tfm\\output\cleaned\\2020-11-01-cleaned.csv",
                 "C:\\Python\\tfm\\output\cleaned\\2020-11-02-cleaned.csv",
                 "C:\\Python\\tfm\\output\cleaned\\2020-11-03-cleaned.csv",
                 "C:\\Python\\tfm\\output\cleaned\\2020-11-04-cleaned.csv",
                 "C:\\Python\\tfm\\output\cleaned\\2020-11-05-cleaned.csv",
                 "C:\\Python\\tfm\\output\cleaned\\2020-11-06-cleaned.csv",
                 "C:\\Python\\tfm\\output\cleaned\\2020-11-07-cleaned.csv",
                 "C:\\Python\\tfm\\output\cleaned\\2020-11-08-cleaned.csv",
                 "C:\\Python\\tfm\\output\cleaned\\2020-11-09-cleaned.csv"]
    """
    csv_files = ["C:\\Python\\tfm\\output\cleaned\\2020-10-28-cleaned.csv",
                 "C:\\Python\\tfm\\output\cleaned\\2020-10-29-cleaned.csv"]"""
    fields = ['tweet.user.description',
              'tweet.user.favourites_count',
              'tweet.user.followers_count',
              'tweet.user.friends_count',
              'tweet.user.id_str',
              'tweet.user.listed_count',
              'tweet.user.location',
              'tweet.user.screen_name',
              'tweet.user.statuses_count',
              'tweet.user.name',
              'tweet.user.verified']
    output_file = "C:\\Python\\tfm\\output\\cleaned\\all_users-cleaned.csv"
    extract_users_info(csv_files, output_file, 'tweet.user.screen_name', fields)