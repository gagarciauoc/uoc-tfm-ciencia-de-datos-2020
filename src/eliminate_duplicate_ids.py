import pandas as pd
import os
import sys


csv_files = [ ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-04-cleaned-retweeted-nodes-2.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-04-cleaned-retweeted-edges-2.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-05-cleaned-retweeted-nodes-2.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-05-cleaned-retweeted-edges-2.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-06-cleaned-retweeted-nodes-2.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-06-cleaned-retweeted-edges-2.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-07-cleaned-retweeted-nodes-2.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-07-cleaned-retweeted-edges-2.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-08-cleaned-retweeted-nodes-2.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-08-cleaned-retweeted-edges-2.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-09-cleaned-retweeted-nodes-2.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-09-cleaned-retweeted-edges-2.csv')]
"""
csv_files = [ ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-09-cleaned-retweeted-nodes-2.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-09-cleaned-retweeted-edges-2.csv')]
"""

csv_ids = 'C:\\Python\\tfm\\changed_names_users.csv'
ref_file = 'C:\\Python\\tfm\\output\\cleaned\\all_users-cleaned.csv'

ref_nodes = pd.read_csv(ref_file, dtype={'tweet.user.id_str': 'str'})
ref_nodes = ref_nodes.drop_duplicates('tweet.user.id_str', keep='last')

for nodes_file, edges_file in csv_files:
    nodes = pd.read_csv(nodes_file, dtype={'tweet.user.id_str': 'str'})
    edges = pd.read_csv(edges_file, dtype={'tweet.user.id_str': 'str', 'tweet.retweeted_status.user.id_str': 'str'})
    ids = pd.read_csv(csv_ids, dtype={'id': 'str'})
    
    splitted = os.path.split(os.path.abspath(nodes_file))
    path = splitted[0]
    filename = splitted[1]
    filename, extension = os.path.splitext(filename)
    retweeted_nodes_file = filename + '-retweeted-nodes-nodup-2' + extension
    retweeted_edges_file = filename + '-retweeted-edges-nodup-2' + extension
    
    nodes = nodes.drop_duplicates('tweet.user.id_str', keep='last')
    
    log = open('log-{}.txt'.format(filename), 'w')
    for id in ids['id']:
        users = ref_nodes[ref_nodes['tweet.user.id_str']==id]
        if len(users)>0:
            # log.write('-'*50+'\n')
            # log.write(id+'\n')
            # log.write(str(users[['tweet.user.id_str', 'tweet.user.screen_name']])+'\n')
            username = users['tweet.user.screen_name'].iloc[-1]
            # log.write(username+'\n')
            # log.write('BEFORE'+'\n')
            # log.write(str(edges.loc[edges['tweet.user.id_str']==id, 'Source'])+'\n')
            # log.write(str(edges.loc[edges['tweet.retweeted_status.user.id_str']==id ,'Target'])+'\n')
            # log.write(str(nodes.loc[nodes['tweet.user.id_str']==id, 'Id'])+'\n')
            # log.write(str(nodes.loc[nodes['tweet.user.id_str']==id, 'Label'])+'\n')
            # 
            edges.loc[edges['tweet.user.id_str']==id, 'Source'] = username
            edges.loc[edges['tweet.retweeted_status.user.id_str']==id ,'Target'] = username
            nodes.loc[nodes['tweet.user.id_str']==id, 'Id'] = username
            nodes.loc[nodes['tweet.user.id_str']==id, 'Label'] = username
            # log.write('AFTER'+'\n')
            # log.write(str(edges.loc[edges['tweet.user.id_str']==id, 'Source'])+'\n')
            # log.write(str(edges.loc[edges['tweet.retweeted_status.user.id_str']==id ,'Target'])+'\n')
            # log.write(str(nodes.loc[nodes['tweet.user.id_str']==id, 'Id'])+'\n')
            # log.write(str(nodes.loc[nodes['tweet.user.id_str']==id, 'Label'])+'\n')
            
    log.close()    
    nodes.to_csv(retweeted_nodes_file, index=False)
    edges.to_csv(retweeted_edges_file, index=False)
        


    