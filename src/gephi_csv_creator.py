import pandas as pd
import datetime
import argparse
import os
import sys


def create_rt_gephi_graph_csv(csv_file, output_path, reference_file=None):
    splitted = os.path.split(os.path.abspath(csv_file))
    path = splitted[0]
    filename = splitted[1]
    filename, extension = os.path.splitext(filename)
    retweeted_nodes_file = output_path + '\\' + filename + '-retweeted-nodes-2' + extension
    retweeted_edges_file = output_path + '\\' + filename + '-retweeted-edges-2' + extension
    df = pd.read_csv(csv_file, dtype={'tweet.user.id_str': 'str', 'tweet.retweeted_status.user.id_str': 'str'})
    #df = df.dropna(subset=['tweet.retweeted_status.user.screen_name'])
    
    nodes_fields = ['tweet.user.description',
                    'tweet.user.favourites_count',
                    'tweet.user.followers_count',
                    'tweet.user.friends_count',
                    'tweet.user.id_str',
                    'tweet.user.listed_count',
                    'tweet.user.location',
                    'tweet.user.screen_name',
                    'tweet.user.statuses_count',
                    'tweet.user.name']
    rt_nodes_fields = [field.replace('tweet.', 'tweet.retweeted_status.') for field in nodes_fields]
    edges_fields = ['tweet.user.screen_name',
                    'tweet.user.id_str',
                    'tweet.place.full_name',
                    'tweet.place.country',
                    'tweet.full_text',
                    'tweet.geo.coordinates',
                    'tweet.retweeted_status.user.screen_name',
                    'tweet.retweeted_status.user.id_str',
                    'tweet.retweeted_status.full_text',
                    'tweet.retweeted_status.source']
    
    if reference_file is not None:
        users_df = pd.read_csv(reference_file, dtype={'tweet.user.id_str': 'str'})
        nodes_df = users_df[nodes_fields].copy()
    else:
        nodes_df = df[nodes_fields].copy()

    edges_df = df[edges_fields].copy()
    edges_df = edges_df.dropna(subset=['tweet.retweeted_status.user.screen_name'])
    
    # Now we add the users info from retweeted status fields to the nodes
    rename_rt_nodes = {}
    for node_field in nodes_fields:
        rename_rt_nodes[node_field.replace('tweet.', 'tweet.retweeted_status.')] = node_field
    rt_nodes_df = df[rt_nodes_fields]
    print(rt_nodes_df.head())
    
    rt_nodes_df = rt_nodes_df.rename(columns=rename_rt_nodes)
    nodes_df = nodes_df.append(rt_nodes_df)


    # We filter only the users that have been retweeted or that have retweeted other user
    from_users = edges_df['tweet.user.screen_name']
    to_users = edges_df['tweet.retweeted_status.user.screen_name']
    from_users_mask = nodes_df['tweet.user.screen_name'].isin(from_users)
    to_users_mask = nodes_df['tweet.user.screen_name'].isin(to_users)
    nodes_from_user_df = nodes_df[from_users_mask].copy()
    nodes_to_user_df = nodes_df[to_users_mask].copy()
    nodes_df = nodes_from_user_df.append(nodes_to_user_df)
    nodes_df = nodes_df.drop_duplicates('tweet.user.screen_name')
    
    # Rename important columns for gephi
    nodes_df['Label'] = nodes_df['tweet.user.screen_name']
    nodes_df = nodes_df.rename(columns={'tweet.user.screen_name': 'Id'})
    edges_df = edges_df.rename(columns={'tweet.user.screen_name': 'Source',
                                        'tweet.retweeted_status.user.screen_name': 'Target'})
    nodes_df.to_csv(retweeted_nodes_file, index=False)
    edges_df.to_csv(retweeted_edges_file, index=False)

    
def run_graph_creator(csv_input_path, csv_output_path, reference_file):
    print('Begin RT graph: {}'.format(datetime.datetime.now()))
    create_rt_gephi_graph_csv(csv_input_path, csv_output_path, reference_file)
    print('End RT graph: {}'.format(datetime.datetime.now()))
        
if __name__ == '__main__':
    # Create the parser
    my_parser = argparse.ArgumentParser(description='Creates Retweet and Quoting graphs CSV files for Gephi based on cleaned Tweet data CSV files')
    # Add the arguments
    my_parser.add_argument('input_csv',
                           metavar='Input csv file',
                           type=str,
                           help='The filename of the input CSV containing the cleaned Tweet data')
    my_parser.add_argument('output_path',
                           metavar='Output Path',
                           type=str,
                           help='The output path to create the new files')
    my_parser.add_argument('--ref',
                           metavar='Users CSV file',
                           type=str,
                           nargs='?',
                           help='CSV file containing a list of users for filtering the RT source and targe users instead of using the actual CSV.')
    # Execute the parse_args() method
    args = my_parser.parse_args()

    csv_input_path = args.input_csv
    csv_output_path = args.output_path
    reference_file = args.ref

    if not os.path.isfile(csv_input_path):
        print('The input file specified does not exist:\n{}'.format(csv_input_path))
        sys.exit()

    if not os.path.isdir(csv_output_path):
        print('The output path specified does not exist:\n{}'.format(csv_output_path))
        sys.exit()
        
    if reference_file is not None and not os.path.isfile(csv_input_path):
        print('The input file specified does not exist:\n{}'.format(csv_input_path))
        sys.exit()
        
    print('\nInput file: {}'.format(csv_input_path))
    print('Output path: {}'.format(csv_output_path))
    print('-'*40 + '\nCreating CSV graph files...')
    run_graph_creator(csv_input_path, csv_output_path, reference_file)
    





    