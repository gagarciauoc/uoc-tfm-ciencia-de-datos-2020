import csv


class TwitterOutputHandler:

    def __init__(self, output):
        self._output = output
        self._init_funcs = {'csv': self._init_output_csv, 'io': self._init_output_io}
        self._end_funcs = {'csv': self._end_output_csv, 'io': self._end_output_io}
        self._write_funcs = {'csv': self._write_output_csv, 'io': self._write_output_io}
    
    def _init_output_csv(self, hashtag, init_date, end_date):
        self._csvfile = open('../output/{}.csv'.format(init_date), 'a', newline='')
        self._csvwriter = csv.writer(self._csvfile)
        self._current_hashtag = hashtag
        self._current_init_date = init_date
        self._current_end_date = end_date
    
    def _end_output_csv(self, hashtag, init_date, end_date):
        self._csvfile.close()
    
    def _init_output_io(self, hashtag, init_date, end_date):
        self._current_hashtag = hashtag
        self._current_init_date = init_date
        self._current_end_date = end_date
        
    def _end_output_io(self, hashtag, init_date, end_date):
        pass
    
    def _write_output_io(self, tweet):
        print('{}\t{}\n{}\n'.format(str(self._current_init_date), str(self._current_hashtag), str(tweet._json)))
    
    def _extract_all_fields(self, level, current_level_name, content, separator):
        for key, value in level.items():
            if isinstance(value, dict):
                self._extract_all_fields(value, current_level_name + '.' + key, content, separator)
            else:
                content.append('{}.{}{}{}'.format(str(current_level_name).encode('utf-8'),
                                                 str(key).encode('utf-8'),
                                                 separator,
                                                 str(value).encode('utf-8')))

    def _write_output_csv(self, tweet):
        tweet_content = []
        current_level_name = 'tweet'
        self._extract_all_fields(tweet._json, current_level_name, tweet_content, separator='$SEP$')
        self._csvwriter.writerow([self._current_init_date, self._current_hashtag] + tweet_content)
        # self._csvwriter.writerow([str(tweet.created_at).encode("utf-8"), str(tweet.created_at).encode("utf-8"), str(tweet.id_str).encode("utf-8"), str(tweet.in_reply_to_status_id_str).encode("utf-8"), str(tweet.user).encode("utf-8"), str(tweet.text).encode("utf-8"), str(tweet.retweeted).encode("utf-8")])

    def init_output(self, hashtag, init_date, end_date):
        self._init_funcs[self._output](hashtag, init_date, end_date)
    
    def end_output(self, hashtag, init_date, end_date):
        self._end_funcs[self._output](hashtag, init_date, end_date)
        
    def write(self, tweet):
        self._write_funcs[self._output](tweet)