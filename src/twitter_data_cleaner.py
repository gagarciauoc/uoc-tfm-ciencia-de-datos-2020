import pandas as pd
import ast
import datetime
import argparse
import os
import sys


class TwitterCSVCleaner:

    def __init__(self, input_csv_path):
        # The input csv should be a dataframe csv built with pandas
        self._input_csv_path = input_csv_path
        self._data_df = pd.read_csv(self._input_csv_path)
        self._clean()
    
    def to_csv(self, output_file_path):
        self._data_df.to_csv(output_file_path, index=False)

    def data(self):
        return self._data_df

    def _fields_from_list(self, list, field):
        result = []
        for elem in list:
            result.append(elem[field])
        return result

    def _clean_literal(self, field_name, field_type):
        if field_type in ('int_list', 'str_list'):
            self._data_df[field_name] = self._data_df[field_name].apply(
                        lambda x: ast.literal_eval(eval(x).decode('utf-8'))
                        if not pd.isnull(x) else x)
        elif field_type=='str':
            self._data_df[field_name] = self._data_df[field_name].apply(
                        lambda x: eval(x) if not pd.isnull(x) and not pd.isna(x) else x)
        elif field_type=='str-utf-8':
            self._data_df[field_name] = self._data_df[field_name].apply(
                        lambda x: x if pd.isnull(x) else ast.literal_eval(x).decode('utf-8'))
        elif field_type=='int':
            self._data_df[field_name] = self._data_df[field_name].apply(
                        lambda x: int(eval(x)) if not pd.isnull(x) else x)
        
    def _clean_list_of_dicts(self, list_name, field_name, field_type='str_list'):
        # 1. Eval and decoding
        self._clean_literal(list_name, field_type)
        # 2. Extract the fields
        self._data_df[list_name] = \
                    self._data_df[list_name].apply(lambda x: self._fields_from_list(x, field_name))
 
    def _clean(self):
        # Drop duplicated tweets based on id_str (we could have extracted the same tweet twice
        # because the tweet contained more than one hashtag on the list
        self._data_df = self._data_df.drop_duplicates('tweet.id_str')
        # Clean relevant fields
        self._clean_list_of_dicts(list_name='tweet.entities.hashtags', field_name='text')
        self._clean_list_of_dicts(list_name='tweet.entities.user_mentions', field_name='screen_name')
        self._clean_literal(field_name='tweet.full_text', field_type='str')
        self._clean_literal(field_name='tweet.retweeted_status.full_text', field_type='str')
        self._clean_literal(field_name='tweet.geo.coordinates', field_type='int_list')
        self._clean_literal(field_name='tweet.place.bounding_box.type', field_type='str-utf-8')
        self._clean_literal(field_name='tweet.place.bounding_box.coordinates', field_type='int_list')
        self._clean_literal(field_name='tweet.retweet_count', field_type='int')
        self._clean_literal(field_name='tweet.retweeted_status.user.id_str', field_type='str-utf-8')
        self._clean_literal(field_name='tweet.retweeted_status.user.screen_name', field_type='str-utf-8')
        self._clean_literal(field_name='tweet.quoted_status.user.screen_name', field_type='str-utf-8')
        self._clean_literal(field_name='tweet.user.followers_count', field_type='int')
        self._clean_literal(field_name='tweet.user.friends_count', field_type='int')
        self._clean_literal(field_name='tweet.user.id_str', field_type='str-utf-8')
        self._clean_literal(field_name='tweet.user.screen_name', field_type='str-utf-8')
        self._clean_literal(field_name='tweet.user.statuses_count', field_type='int')
        self._clean_literal(field_name='tweet.retweeted_status.user.followers_count', field_type='int')
        self._clean_literal(field_name='tweet.retweeted_status.user.friends_count', field_type='int')
        self._clean_literal(field_name='tweet.retweeted_status.user.statuses_count', field_type='int')


def run_cleaner(csv_input_path, csv_output_path):
    print('Begin clean: {}'.format(datetime.datetime.now()))
    cleaner = TwitterCSVCleaner(csv_input_path)
    tweets_df = cleaner.data()
    print('End clean: {}'.format(datetime.datetime.now()))
    print('Begin export: {}'.format(datetime.datetime.now()))
    tweets_df.to_csv(csv_output_path)
    print('End export: {}'.format(datetime.datetime.now()))
        
if __name__ == '__main__':
    # Create the parser
    my_parser = argparse.ArgumentParser(description='Cleans a CSV file containing Tweets processed data')
    # Add the arguments
    my_parser.add_argument('input_csv',
                           metavar='Input csv file',
                           type=str,
                           help='The filename of the input CSV containing the processed data')
    my_parser.add_argument('output_csv',
                           metavar='Output csv file',
                           type=str,
                           help='The filename of the output CSV that will hold the cleaned Tweets data')
    # Execute the parse_args() method
    args = my_parser.parse_args()

    csv_input_path = args.input_csv
    csv_output_path = args.output_csv

    if not os.path.isfile(csv_input_path):
        print('The input file specified does not exist:\n{}'.format(csv_input_path))
        sys.exit()

    out_path = os.path.split(os.path.abspath(csv_output_path))[0]
    if not os.path.isdir(out_path):
        print('The output path specified does not exist:\n{}'.format(out_path))
        sys.exit()
        
    print('\nInput file: {}'.format(csv_input_path))
    print('Output file: {}'.format(csv_output_path))
    print('-'*40 + '\nCleaning...')
    run_cleaner(csv_input_path, csv_output_path)