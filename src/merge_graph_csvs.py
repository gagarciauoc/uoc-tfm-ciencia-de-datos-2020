import pandas as pd
import sys


def merge_graphs(csv_input_files, output_files):
    print('Merging graphs:\n{}'.format(csv_input_files))
    print('\n\nOutput files:\n{}'.format(output_files))
    graph_nodes_df = pd.DataFrame()
    graph_edges_df = pd.DataFrame()
    for files_pair in csv_input_files:
        nodes = files_pair[0]
        edges = files_pair[1]
        nodes_df = pd.read_csv(nodes, dtype={'tweet.user.id_str': 'str'})
        edges_df = pd.read_csv(edges, dtype={'tweet.user.id_str': 'str', 'tweet.retweeted_status.user.id_str': 'str'})
        graph_nodes_df = graph_nodes_df.append(nodes_df, ignore_index=True)
        graph_edges_df = graph_edges_df.append(edges_df, ignore_index=True)
        # Drop duplicates
        graph_nodes_df = graph_nodes_df.drop_duplicates('Id', keep='last')
        graph_nodes_df = graph_nodes_df.drop_duplicates('tweet.user.id_str', keep='last')
    graph_nodes_df.to_csv(output_files[0], index=False)
    graph_edges_df.to_csv(output_files[1], index=False)

if __name__ == '__main__':
    """
    files = [ ('C:\\Python\\tfm\\output\\gephi_csv\\2020-10-28-cleaned-retweeted-nodes.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-10-28-cleaned-retweeted-edges.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-10-29-cleaned-retweeted-nodes.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-10-29-cleaned-retweeted-edges.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-10-30-cleaned-retweeted-nodes.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-10-30-cleaned-retweeted-edges.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-10-31-cleaned-retweeted-nodes.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-10-31-cleaned-retweeted-edges.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-01-cleaned-retweeted-nodes.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-01-cleaned-retweeted-edges.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-02-cleaned-retweeted-nodes.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-02-cleaned-retweeted-edges.csv')]
    output_files = ('C:\\Python\\tfm\\output\\gephi_csv\\_Campaign_201028_201102-retweeted-nodes.csv',
                    'C:\\Python\\tfm\\output\\gephi_csv\\_Campaign_201028_201102-retweeted-edges.csv')
    merge_graphs(files, output_files)
    """
    """
    files = [ ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-04-cleaned-retweeted-nodes-2.csv',
           'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-04-cleaned-retweeted-nodes-2.csv'),
           ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-05-cleaned-retweeted-nodes-2.csv',
           'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-05-cleaned-retweeted-nodes-2.csv'),
           ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-06-cleaned-retweeted-nodes-2.csv',
           'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-06-cleaned-retweeted-nodes-2.csv'),
           ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-07-cleaned-retweeted-nodes-2.csv',
           'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-07-cleaned-retweeted-nodes-2.csv'),
           ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-08-cleaned-retweeted-nodes-2.csv',
           'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-08-cleaned-retweeted-nodes-2.csv'),
           ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-09-cleaned-retweeted-nodes-2.csv',
           'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-09-cleaned-retweeted-nodes-2.csv')]
    
    """
    files = [ ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-04-cleaned-retweeted-nodes-2-retweeted-nodes-nodup-2.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-04-cleaned-retweeted-nodes-2-retweeted-edges-nodup-2.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-05-cleaned-retweeted-nodes-2-retweeted-nodes-nodup-2.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-05-cleaned-retweeted-nodes-2-retweeted-edges-nodup-2.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-06-cleaned-retweeted-nodes-2-retweeted-nodes-nodup-2.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-06-cleaned-retweeted-nodes-2-retweeted-edges-nodup-2.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-07-cleaned-retweeted-nodes-2-retweeted-nodes-nodup-2.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-07-cleaned-retweeted-nodes-2-retweeted-edges-nodup-2.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-08-cleaned-retweeted-nodes-2-retweeted-nodes-nodup-2.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-08-cleaned-retweeted-nodes-2-retweeted-edges-nodup-2.csv'),
               ('C:\\Python\\tfm\\output\\gephi_csv\\2020-11-09-cleaned-retweeted-nodes-2-retweeted-nodes-nodup-2.csv',
               'C:\\Python\\tfm\\output\\gephi_csv\\2020-11-09-cleaned-retweeted-nodes-2-retweeted-edges-nodup-2.csv')]
    
    output_files = ('C:\\Python\\tfm\\output\\gephi_csv\\_PostCampaign_201104_201109-retweeted-nodes-2.csv',
                    'C:\\Python\\tfm\\output\\gephi_csv\\_PostCampaign_201104_201109-retweeted-edges-2.csv')
    merge_graphs(files, output_files)
