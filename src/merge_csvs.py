import pandas as pd
        

if __name__ == '__main__':
    files = [ 'C:\\Python\\tfm\\output\\cleaned\\2020-11-04_0-cleaned.csv',
              'C:\\Python\\tfm\\output\\cleaned\\2020-11-04_1-cleaned.csv',
              'C:\\Python\\tfm\\output\\cleaned\\2020-11-04_2-cleaned.csv',
              'C:\\Python\\tfm\\output\\cleaned\\2020-11-04_#Election2020-cleaned.csv']
    output_file = 'C:\\Python\\tfm\\output\\cleaned\\2020-11-04-cleaned.csv'
    output_df = pd.DataFrame()
    for file in files:
        current_df = pd.read_csv(file, dtype={'tweet.user.id_str': 'str', 'tweet.retweeted_status.user.id_str': 'str'})
        output_df = output_df.append(current_df, ignore_index=True)
    output_df = output_df.drop_duplicates('tweet.id_str')
    output_df.to_csv(output_file, index=False)