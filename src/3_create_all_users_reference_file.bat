rem Extract users from all the days so we build a master CSV file that will be a reference for the graph creation
rem This is needed as there are RT's that have been done in different days than the original tweet was published from. So we have a more complete
rem users database for the graphs
python extract_users.py